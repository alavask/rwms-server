﻿using System;
using System.Linq;
using Mephi.Cybernetics.RWMS.Server.Models.LifeCycle;
using Mephi.Cybernetics.RWMS.Server.Models.LifeCycle.Templates;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Mephi.Cybernetics.RWMS.Server.Controllers.Projects
{
    [ApiController]
    [Route("[controller]/[action]/")]
    public class ProjectController : ControllerBase
    {
        [HttpPost]
        public int CreateUIR([FromBody] UIRCreationDto dto)
        {
            var project = UIRTemplate.Create(
                dto.StudentName,
                dto.TeacherName,
                dto.ConsultantName,
                dto.StudentGroup,
                dto.TeacherGroup,
                dto.ConsultantGroup,
                dto.Start,
                dto.DoTask,
                dto.DoRSPZ,
                dto.DoPZ,
                dto.Theme
            );
            return project.Id;
        }

        [HttpDelete]
        [Route("{projectId}")]
        public bool Remove(int projectId)
        {
            using var ctx = new ApplicationContext();
            var project = ctx.Projects
                .Include(p => p.Process)
                .ThenInclude(p => p.Stages)
                .ThenInclude(s => s.Gate)
                .Include(p => p.Process)
                .ThenInclude(p => p.Responsibilities)
                .FirstOrDefault(p => p.Id == projectId);
            if (project == null) { return false; }
            ctx.Remove(project);
            ctx.SaveChanges();
            return true;
        }

        [HttpGet]
        public MiniProjectDto[] GetProjects()
        {
            using var ctx = new ApplicationContext();
            return ctx.Projects.AsNoTracking()
                .Include(p => p.Process)
                .Select(p => new MiniProjectDto(p))
                .ToArray();
        }
        
        [HttpGet]
        [Route("{projectId}")]
        public FullProjectDto GetProject(int projectId)
        {
            using var ctx = new ApplicationContext();
            var project = ctx.Projects
                .Include(p => p.Process)
                .ThenInclude(p => p.Stages)
                .ThenInclude(s => s.Gate)
                .FirstOrDefault(p => p.Id == projectId);
            return new FullProjectDto(project);
        }

        [HttpGet]
        [Route("{projectId}")]
        public Person[] GetAssignees(int projectId)
        {
            using var ctx = new ApplicationContext();
            var project = ctx.Projects.AsNoTracking()
                .Include(p => p.Process)
                .ThenInclude(p => p.Responsibilities)
                .ThenInclude(r => r.Assignee)
                .FirstOrDefault(p => p.Id == projectId);
            if (project == null)
            {
                return new Person[] { };
            }
            var assignees = project.Process.Responsibilities
                .Select(r => r.Assignee)
                .Where(a => a.Name != "")
                .Distinct();
            return assignees.ToArray();
        }

        [HttpGet]
        [Route("{projectId}")]
        public Stage[] GetActiveStages(int projectId)
        {
            using var ctx = new ApplicationContext();
            var stages = ctx.Projects.AsNoTracking()
                .Include(p => p.Process)
                .ThenInclude(p => p.Stages)
                .Where(p => p.Id == projectId)
                .SelectMany(p => p.Process.Stages)
                .Where(s => s.State == StageState.InProgress);
            return stages.ToArray();
        }

        [HttpGet]
        [Route("{projectId}/{operationId}")]
        public Guid[] GetOperationResults(int projectId, int operationId)
        {
            using var ctx = new ApplicationContext();
            var project = ctx.Projects.AsNoTracking()
                .Include(p => p.Process)
                .ThenInclude(p => p.Stages)
                .ThenInclude(s => s.OperationInstance)
                .ThenInclude(o => o.Results)
                .FirstOrDefault(p => p.Id == projectId);
            if (project is null) { return new Guid[] { }; }
            var guids = project.Process.Stages
                .SelectMany(s => s.OperationInstance.Results.Select(a => a.Link))
                .ToArray();
            return guids;
        }

        [HttpGet]
        [Route("{projectId}/{operationId}")]
        public Guid[] GetOperationArguments(int projectId, int operationId)
        {
            using var ctx = new ApplicationContext();
            var project = ctx.Projects.AsNoTracking()
                .Include(p => p.Process)
                .ThenInclude(p => p.Stages)
                .ThenInclude(s => s.OperationInstance)
                .ThenInclude(o => o.Arguments)
                .FirstOrDefault(p => p.Id == projectId);
            if (project is null) { return new Guid[] { }; }
            var guids = project.Process.Stages
                .SelectMany(s => s.OperationInstance.Results.Select(a => a.Link))
                .ToArray();
            return guids;
        }

        [HttpPut]
        [Route("{projectId}")]
        public bool Start(int projectId)
        {
            using var ctx = new ApplicationContext();
            var project = ctx.Projects
                .Include(p => p.Process)
                .ThenInclude(p => p.Stages)
                .ThenInclude(s => s.Gate)
                .FirstOrDefault(p => p.Id == projectId);
            if (project == null) { return false; }
            var result = project.Process.Start();
            ctx.SaveChanges();
            return result;
        }

        /// <summary>
        /// Сообщает серверу, что выполнилась некоторая операция, а результатом операции является некоторое событие.
        /// В случае, если не требуется генерировать событие по окончании операции, то необходимо передавать
        /// <code>eventId = -1</code>.
        /// </summary>
        /// <param name="projectId">ID проекта, для которого нужно выполнить операцию</param>
        /// <param name="operationId">ID выполняемой операции</param>
        /// <param name="eventId">ID события, генерируемого в результате выполнения операции; если нет такого события,
        /// то передаётся -1</param>
        /// <returns>Сообщение об успехе выполнения операции.</returns>
        [HttpPut]
        [Route("{projectId}/{operationId}/{eventId}")]
        public bool CompleteOperationWithEvent(int projectId, int operationId, int eventId)
        {
            using var ctx = new ApplicationContext();
            var project = ctx.Projects
                .Include(p => p.Process)
                .ThenInclude(p => p.Stages)
                .ThenInclude(s => s.Gate)
                .FirstOrDefault(p => p.Id == projectId);
            var operation = project?.Process.Stages.FirstOrDefault(s => s.Id == operationId);
            if (operation?.OperationInstance is null || operation.State != StageState.InProgress) { return false; }
            operation.State = StageState.Done;
            if (eventId != -1)
            {
                var @event = project.Process.Stages.FirstOrDefault(s => s.Id == eventId);
                if (@event?.EventInstance is null) { return false; }
                @event.EventInstance.Occured = true;
            }
            ctx.SaveChanges();
            project.Process.Propagate();
            ctx.SaveChanges();
            return true;
        }
    }
}