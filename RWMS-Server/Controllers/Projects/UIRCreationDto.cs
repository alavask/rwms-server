﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mephi.Cybernetics.RWMS.Server.Models.LifeCycle;

namespace Mephi.Cybernetics.RWMS.Server.Controllers.Projects
{
    public class UIRCreationDto
    {
        public UIRCreationDto(string studentName, string studentGroup, string teacherName, string teacherGroup,
            string consultantName, string consultantGroup, string theme, ProjectType projetType, DateTime start,
            DateTime doTask, DateTime doRspz, DateTime doPz)
        {
            StudentName = studentName;
            StudentGroup = studentGroup;
            TeacherName = teacherName;
            TeacherGroup = teacherGroup;
            ConsultantName = consultantName;
            ConsultantGroup = consultantGroup;
            Theme = theme;
            ProjetType = projetType;
            Start = start;
            DoTask = doTask;
            DoRSPZ = doRspz;
            DoPZ = doPz;
        }

        public string StudentName { get; set; } // TODO: change to user id, when auth-service added
        public string StudentGroup { get; set; }
        public string TeacherName { get; set; } // TODO: change to user id, when auth-service added
        public string TeacherGroup { get; set; }
        public string ConsultantName { get; set; } // TODO: change to user id, when auth service added
        public string ConsultantGroup { get; set; }
        public string Theme { get; set; }
        public ProjectType ProjetType { get; set; }
        public DateTime Start { get; set; }
        public DateTime DoTask { get; set; }
        public DateTime DoRSPZ { get; set; }
        public DateTime DoPZ { get; set; }
    }
}