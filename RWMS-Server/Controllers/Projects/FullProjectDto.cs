﻿using System;
using System.Globalization;
using System.Linq;
using Mephi.Cybernetics.RWMS.Server.Models.LifeCycle;

namespace Mephi.Cybernetics.RWMS.Server.Controllers.Projects
{
    public class FullProjectDto
    {
        public int                 ProjectId          { get; }
        public string              Theme              { get; }
        public int                 ParentProjectId    { get; }
        public string              ProjectType        { get; }
        public int                 ProcessId          { get; }
        public string              ProcessName        { get; }
        public string              ProcessDescription { get; }
        public string              ProcessStatus      { get; }
        public int                 FirstStageId       { get; }
        public StageDto[]          Stages             { get; }
        public ResponsibilityDto[] Responsibilities   { get; }

        public FullProjectDto(Project project)
        {
            this.ProjectId = project.Id;
            this.Theme = project.Theme;
            this.ParentProjectId = project.Parent?.Id ?? -1;
            this.ProjectType = project.Type.ToString();

            this.ProcessId = project.Process?.Id ?? -1;
            this.ProcessName = project.Process?.Name ?? "";
            this.ProcessDescription = project.Process?.Description ?? "";
            this.ProcessStatus = project.Process?.Status.ToString() ?? "";
            this.FirstStageId = project.Process?.FirstStage?.Id ?? -1;
            if (project.Process == null)
            {
                this.Stages = null;
                this.Responsibilities = null;
                return;
            }
            if (project.Process.Stages == null || project.Process.Stages.Count == 0)
            {
                this.Stages = null;
            }
            else
            {
                var stages = new StageDto[project.Process.Stages.Count];
                var i = 0;
                foreach (var stage in project.Process.Stages)
                {
                    stages[i] = new StageDto(stage);
                    i++;
                }
                this.Stages = stages;
            }
            if (project.Process.Responsibilities == null || project.Process.Responsibilities.Count == 0)
                this.Responsibilities = null;
            else
            {
                var responsibilities = new ResponsibilityDto[project.Process.Responsibilities.Count];
                var i = 0;
                foreach (var resp in project.Process.Responsibilities)
                {
                    responsibilities[i] = new ResponsibilityDto(resp);
                    i++;
                }
                this.Responsibilities = responsibilities;
            }
        }
    }

    public class StageDto
    {
        public int    StageId          { get; }
        public string StageName        { get; }
        public string StageDescription { get; }
        public string StageState       { get; }
        public string DueDate          { get; }

        public string StageType { get; }

        public bool   Occured   { get; }
        public string EventType { get; }

        public ArtifactDto[] Arguments { get; }
        public ArtifactDto[] Results   { get; }

        public int    GateId           { get; }
        public string GateLogicalType  { get; }
        public string GateType         { get; }
        public int[]  PreviousStageIds { get; }
        public int[]  NextStageIds     { get; }

        public StageDto(Stage stage)
        {
            this.StageId = stage.Id;
            this.StageName = stage.Name;
            this.StageDescription = stage.Description;
            this.StageState = stage.State.ToString();
            this.DueDate = stage.DueDate.ToString(CultureInfo.InvariantCulture);

            if (stage.EventInstance != null)
            {
                var ev = stage.EventInstance;
                this.StageType = nameof(Event);
                this.Occured = ev.Occured;
                this.EventType = ev.Type.ToString();
                this.Arguments = null;
                this.Results = null;
            }
            if (stage.OperationInstance != null)
            {
                var op = stage.OperationInstance;
                this.StageType = nameof(Operation);
                if (op.Arguments == null || op.Arguments.Count == 0) { this.Arguments = null; }
                else
                {
                    var arguments = new ArtifactDto[op.Arguments.Count];
                    var i = 0;
                    foreach (var artifact in op.Arguments)
                    {
                        arguments[i] = new ArtifactDto(artifact);
                        i++;
                    }
                    this.Arguments = arguments;
                }
                if (op.Results == null || op.Results.Count == 0) { this.Results = null; }
                else
                {
                    var results = new ArtifactDto[op.Results.Count];
                    var i = 0;
                    foreach (var artifact in op.Results)
                    {
                        results[i] = new ArtifactDto(artifact);
                        i++;
                    }
                    this.Results = results;
                }
                this.Occured = false;
                this.EventType = "";
            }

            if (stage.Gate == null)
            {
                this.GateType = "";
                this.GateLogicalType = "";
                this.PreviousStageIds = null;
                this.NextStageIds = null;
                return;
            }
            this.GateId = stage.Gate.Id;
            this.GateType = stage.Gate.ConnectionType.ToString();
            this.GateLogicalType = stage.Gate.LogicalType.ToString();
            this.PreviousStageIds = stage.Gate.Previous.Select(x => x.Id).ToArray();
            this.PreviousStageIds = stage.Gate.Next.Select(x => x.Id).ToArray();
        }
    }

    public class ArtifactDto
    {
        public int    ArtifactId          { get; }
        public string ArtifactName        { get; }
        public string ArtifactDescription { get; }
        public Guid Link { get; }

        public ArtifactDto(Artifact artifact)
        {
            this.ArtifactId = artifact.Id;
            this.ArtifactName = artifact.Name;
            this.ArtifactDescription = artifact.Description;
            this.Link = artifact.Link;
        }
    }

    public class ResponsibilityDto
    {
        public int    ResponsibilityId    { get; }
        public int    OperationId         { get; }
        public int    AssigneeId          { get; }
        public string AssigneeName        { get; }
        public string AssigneeDescription { get; }
        public string AssigneeRole        { get; }
        public string AssigneeGroup       { get; }

        public ResponsibilityDto(Responsibility responsibility)
        {
            this.ResponsibilityId = responsibility.Id;
            this.OperationId = responsibility.Operation?.Id ?? -1;
            this.AssigneeId = responsibility.Assignee?.Id ?? -1;
            this.AssigneeDescription = responsibility.Assignee?.Description ?? "";
            this.AssigneeGroup = responsibility.Assignee?.Group ?? "";
            this.AssigneeName = responsibility.Assignee?.Name ?? "";
            this.AssigneeRole = responsibility.Assignee?.Role ?? "";
        }
    }
}