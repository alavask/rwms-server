﻿using Mephi.Cybernetics.RWMS.Server.Models.LifeCycle;

namespace Mephi.Cybernetics.RWMS.Server.Controllers.Projects
{
    public class MiniProjectDto
    {
        public int ProjectId { get; }
        public string Theme { get; }
        public string ProjectType { get; }
        public string ProcessName { get; }
        public string ProcessStatus { get; }

        public MiniProjectDto(Project project)
        {
            this.ProjectId = project.Id;
            this.Theme = project.Theme;
            this.ProjectType = project.Type.ToString();
            this.ProcessName = project.Process.Name;
            this.ProcessStatus = project.Process.Status.ToString();
        }
    }
}