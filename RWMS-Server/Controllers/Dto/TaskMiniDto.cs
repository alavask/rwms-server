﻿namespace Mephi.Cybernetics.RWMS.Server.Controllers.Dto
{
    public class TaskMiniDto
    {
        public TaskMiniDto(int id, string theme, string studentName, string teacherName, string date)
        {
            this.Id = id;
            this.Theme = theme;
            this.StudentName = studentName;
            this.TeacherName = teacherName;
            this.Date = date;
        }

        public int Id { get; }
        public string Theme { get; }
        public string StudentName { get; }
        public string TeacherName { get; }
        public string Date { get; }
    }
}