﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mephi.Cybernetics.RWMS.Server.Models.Documents;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Mephi.Cybernetics.RWMS.Server.Controllers
{
    [Route("[controller]/[action]/")]
    public class RSPZsController : Controller
    {
        [HttpGet("{id}")]
        public RSPZ Get(int id)
        {
            using var ctx = new ApplicationContext();
            var result = ctx.RSPZs.AsNoTracking()
                .Include(x => x.Chapters)
                .ThenInclude(x => x.Sections)
                .Where(x => x.Id == id).FirstOrDefault();
            return result;
        }

        [HttpPut]
        public void Update([FromBody]RSPZ value)
        {
            using var ctx = new ApplicationContext();
            ctx.RSPZs.Update(value);
            ctx.SaveChanges();
        }
    }
}
