﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mephi.Cybernetics.RWMS.Server.Controllers.Dto;
using Mephi.Cybernetics.RWMS.Server.Models.Tasks;
using Mephi.Cybernetics.RWMS.Server.Models.Tasks.Templates;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Mephi.Cybernetics.RWMS.Server.Controllers
{
	[ApiController]
	[Route("[controller]/[action]/")]
	public class TaskController : ControllerBase
	{
		[HttpGet]
		[Route("{taskId}")]
		public Task Get(int taskId)
		{
			using var ctx = new ApplicationContext();
			var task = ctx.Tasks.AsNoTracking()
				.Include(t => t.Chapters)
				.ThenInclude(c => c.Sections)
				.Include(t => t.Books)
				.FirstOrDefault(t => t.Id == taskId);
			return task;
		}
		
		[HttpGet]
		[Route("{taskId}")]
		public Task[] GetVersions(int taskId)
		{
			using var ctx = new ApplicationContext();
			var task = ctx.Tasks.AsNoTracking().FirstOrDefault(t => t.Id == taskId);
			var versions = ctx.Tasks.AsNoTracking()
				.Where(t => t.Guid == task.Guid);
			return versions.ToArray();
		}

		[HttpGet]
		public TaskMiniDto[] GetNewestVersionsInfo()
		{
			var result = new List<TaskMiniDto>();
			using var ctx = new ApplicationContext();
			var tasks = ctx.Tasks.AsNoTracking().ToList();
			var guids = tasks.Select(t => t.Guid).Distinct();
			foreach (var guid in guids)
			{
				var versions = tasks.Where(t => t.Guid == guid).ToList();
				var lastDate = versions.Max(t => t.CreatedDate);
				var newest = versions.FirstOrDefault(t => t.CreatedDate == lastDate);
				result.Add(new TaskMiniDto(newest.Id, newest.Theme, newest.StudentName, newest.TeacherName, newest.Date));
			}
			return result.ToArray();
		}

		/*
		[HttpPut]
		public bool Save([FromBody] Task clientTask)
		{
			Console.WriteLine($"BEFORE: {clientTask.Id}");
			using var ctx = new ApplicationContext();
			var serverTask = ctx.Tasks.AsNoTracking()
				.Include(t => t.Chapters)
				.ThenInclude(c => c.Sections)
				.Include(t => t.Books)
				.FirstOrDefault(t => t.Id == clientTask.Id);
			foreach(var chapter in serverTask.Chapters)
			{
				if (clientTask.Chapters.Contains(chapter) == false)
				{
					ctx.
				}
			}
			Console.WriteLine($"AFTER: {clientTask.Id}");
			return true;
		}
		*/

		[HttpPost]
		public Task Create()
		{
			using var ctx = new ApplicationContext();
			var task = UIRTaskTemplate.Make();
			ctx.Tasks.Add(task);
			ctx.SaveChanges();
			return task;
		}
		
		[HttpPost]
		public int Save([FromBody] Task clientTask)
		{
			using var ctx = new ApplicationContext();
			var newVersion = clientTask.Clone() as Task;
			ctx.Tasks.Add(newVersion);
			ctx.SaveChanges();
			return newVersion.Id;
		}

		[HttpDelete]
		[Route("{taskId}")]
		public bool Remove(int taskId)
		{
			using var ctx = new ApplicationContext();
			var task = ctx.Tasks.FirstOrDefault(t => t.Id == taskId);
			var tasks = ctx.Tasks
				.Include(t => t.Chapters)
				.ThenInclude(c => c.Sections)
				.Include(t => t.Books)
				.Where(t => t.Guid == task.Guid);
			ctx.RemoveRange(tasks);	
			ctx.SaveChanges();
			return true;
		}
	}
}
