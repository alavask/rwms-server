﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mephi.Cybernetics.RWMS.Server.Models.Tasks.Templates
{
	public static class UIRTaskTemplate
	{
		public static Task Make()
		{
			var task = new Task()
			{
				Chapters = new List<Chapter>()
				{
					new Chapter() {Name = "Аналитическая часть"},
					new Chapter() {Name = "Теоретическая часть"},
					new Chapter() {Name = "Инженерная часть"},
					new Chapter() {Name = "Практическая часть"}
				},
				Date = $"{DateTime.Today.Day}.{DateTime.Today.Month}.{DateTime.Today.Year}",
			};
			return task;
		}
		
		public static Task CreateDefault()
		{
			var task = new Task()
			{
				Group = "Б16-504",
				StudentName = "Ааамов Бббек Вввахтангович",
				Theme = "Гггрокание алгоритмов",
				Chapters = new List<Chapter>
				{
					new Chapter
					{
						Name = "Аналитическая часть",
						Sections = new List<Section>
						{
							new Section
							{
								Name = "Изучение и сравнительный анализ определений алгоритма",
								Date = "22.09.2019",
								ReportTypes = "Аналитический обзор, Разделы ПЗ"
							},
							new Section
							{
								Name = "Оформление расширенного содержания пояснительной записки (РСПЗ)",
								Date = "11.10.2019",
								ReportTypes = "Разделы ПЗ"
							}
						}
					},
					new Chapter
					{
						Name = "Теоретическая часть",
						Sections = new List<Section>
						{
							new Section
							{
								Name = "Разработка структур очень важных данных",
								Date = "11.10.2019",
								ReportTypes = "Модель, Разделы ПЗ"
							},
							new Section
							{
								Name = "Разработка структур других важных данных",
								Date = "11.10.2019",
								ReportTypes = "Модель, Разделы ПЗ"
							}
						}
					},
					new Chapter
					{
						Name = "Инженерная часть",
						Sections = new List<Section>
						{
							new Section
							{
								Name = "Проектирование алгоритма в первом приближении",
								Date = "22.11.2019",
								ReportTypes = "Архитектура,Разделы ПЗ"
							},
							new Section
							{
								Name = "Что-то тоже напроектировали",
								Date = "22.11.2019",
								ReportTypes = "API, Разделы ПЗ"
							}
						}
					},
					new Chapter
					{
						Name = "Практическая часть",
						Sections = new List<Section>
						{
							new Section
							{
								Name = "Реализация номер раз",
								Date = "05.12.2019",
								ReportTypes = "Исходный код, Разделы ПЗ"
							},
							new Section
							{
								Name = "Реализация номер два",
								Date = "15.12.2019",
								ReportTypes = "Исходный код, Разделы ПЗ"
							},
							new Section
							{
								Name = "Тестеки",
								Date = "20.12.2019",
								ReportTypes = "Тесты, Разделы ПЗ"
							}
						}
					}

				},
				Books = new List<Book>
				{
					new Book
					{
						Name = "Книга первая",
						Description = "Описание первой книги"
					},
				},
				Date = "10.09.2019",
				TeacherName = "Ддденисов Ееевгений Ёёёжикович"
			};
			using var context = new ApplicationContext();
			context.Tasks.Add(task);
			context.SaveChanges();
			return task;
		}
	}
}
