﻿using Mephi.Cybernetics.RWMS.Server.Models.LifeCycle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mephi.Cybernetics.RWMS.Server.Models.Tasks
{
	public class Task : NamedEntity, ICloneable
	{
		public virtual string Theme { get; set; }
		public virtual string Group { get; set; }
		public virtual string StudentName { get; set; }
		public virtual ICollection<Chapter> Chapters { get; set; } = new List<Chapter>();
		public virtual ICollection<Book> Books { get; set; } = new List<Book>();
		public virtual string Date { get; set; }
		public virtual string TeacherName { get; set; }
		public virtual Guid Guid { get; set; } = Guid.NewGuid();
		public virtual DateTime CreatedDate { get; set; } = DateTime.Now;

		public object Clone()
		{
			var result = new Task
			{
				Name = this.Name,
				Description = this.Description,
				Theme = this.Theme,
				Group = this.Group,
				StudentName = this.StudentName,
				TeacherName = this.TeacherName,
				Date = this.Date,
			};
			foreach (var chapter in this.Chapters)
			{
				var c = new Chapter
				{
					Name = chapter.Name,
				};
				foreach (var section in chapter.Sections)
				{
					var s = new Section
					{
						Name = section.Name,
						Date = section.Date,
						ReportTypes = section.ReportTypes
					};
					c.Sections.Add(s);
				}
				result.Chapters.Add(c);
			}
			foreach(var book in this.Books)
			{
				var b = new Book
				{
					Name = book.Name,
					Description = book.Description
				};
				result.Books.Add(b);
			}
			result.Guid = this.Guid;
			return result;
		}
	}
}