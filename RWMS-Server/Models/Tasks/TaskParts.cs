﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mephi.Cybernetics.RWMS.Server.Models.Tasks
{
	public class Chapter : BaseEntity
	{
		public virtual ICollection<Section> Sections { get; set; } = new List<Section>();
		public virtual string Name { get; set; }

		public override bool Equals(object obj)
		{
			if (!(obj is Chapter chapter)) { return false; }
			return chapter.Id == this.Id;
		}
	}

	public class Section : BaseEntity
	{
		public virtual string Name { get; set; }
		public virtual string Date { get; set; }
		public virtual string ReportTypes { get; set; }

		public override bool Equals(object obj)
		{
			if (!(obj is Section section)) { return false; }
			return section.Id == this.Id;
		}
	}

	public class Book : NamedEntity { }
}
