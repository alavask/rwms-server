﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mephi.Cybernetics.RWMS.Server.Models
{
	public abstract class NamedEntity : BaseEntity
	{
		public virtual string Name { get; set; }
		public virtual string Description { get; set; }
	}
}
