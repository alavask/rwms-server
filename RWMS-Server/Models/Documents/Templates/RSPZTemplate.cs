﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mephi.Cybernetics.RWMS.Server.Models.Documents.Templates
{
    public static class RSPZTemplate
    {
        public static RSPZ Create()
        {
            var rspz = new RSPZ()
            {
                Theme = "Тема",
                Essay = "Заголовок Первый абзац с курсивным шрифтом, жирным выделением и жирным курсивом.#include <iostream> using namespace std;",
                Introduction = "Введение",
                Conclusion = "Заключение",
                Literature = "Список литературы",
                Chapters = new List<Chapter>
                {
                    new Chapter
                    {
                        Header = "Заголовок первого раздела",
                        Annotation = "Аннотация первого раздела",
                        Sections = new List<Section>
                        {
                            new Section
                            {
                                Header = "Заголовок первого подраздела",
                                Annotation = "Аннотация первого подраздела",
                                Content = "Контент первого подраздела"
                            },
                            new Section
                            {
                                Header = "2",
                                Annotation = "2",
                                Content = "2"
                            },
                            new Section
                            {
                                Header = "3",
                                Annotation = "3",
                                Content = "3"
                            }
                        }
                    },
                    new Chapter
                    {
                        Header = "1",
                        Annotation = "132",
                        Sections = new List<Section>
                        {
                            new Section
                            {
                                Header = "13",
                                Annotation = "23",
                                Content = "3"
                            },                      
                        }
                    }
                }
            };
            using var ctx = new ApplicationContext();
            ctx.RSPZs.Add(rspz);
            ctx.SaveChanges();
            return rspz;
        }
    }
}
