﻿using System;
using System.Collections.Generic;
using System.Text;
using Mephi.Cybernetics.RWMS.Server.Models.LifeCycle;

namespace Mephi.Cybernetics.RWMS.Server.Models.Documents
{
	public class PZ : Artifact
	{
		public virtual string Theme { get; set; }
		public virtual string Introduction { get; set; }
		public virtual string Conclusion { get; set; }
		public virtual ICollection<Chapter> Chapters { get; set; }
	}
}
