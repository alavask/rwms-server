﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mephi.Cybernetics.RWMS.Server.Models.Documents
{
	public class Chapter : BaseEntity
	{
		public virtual string Header { get; set; }
		public virtual string Annotation { get; set; }
		public virtual ICollection<Section> Sections { get; set; }
	}
}
