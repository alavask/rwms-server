﻿using System;

namespace Mephi.Cybernetics.RWMS.Server.Models.LifeCycle
{
	public class Artifact : NamedEntity
	{
		public virtual Guid Link { get; set; }
	}
}
