﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mephi.Cybernetics.RWMS.Server.Models.LifeCycle
{
	public class Person : NamedEntity
	{
		public virtual string Role { get; set; }

		public virtual string Group { get; set; }

		public override bool Equals(object obj)
		{
			return obj is Person person && this.Id == person.Id;
		}

		public override int GetHashCode()
		{
			return this.Id.GetHashCode();
		}

		public override string ToString()
		{
			return $"Person:{{ Id={this.Id} Name=\'{this.Name}\' Role=\'{this.Role}\' Group=\'{this.Group}\' }}";
		}
	}
}
