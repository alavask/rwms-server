﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mephi.Cybernetics.RWMS.Server.Models.LifeCycle
{
    public class Stage : NamedEntity
    {
        public virtual StageState State             { get; set; } = StageState.Open;
        public virtual DateTime   DueDate           { get; set; } = new DateTime();
        public virtual Gate       Gate              { get; set; }
        public virtual Event      EventInstance     { get; set; }
        public virtual Operation  OperationInstance { get; set; }

        public bool AsEvent(out Event @event)
        {
            if (this.EventInstance is null)
            {
                @event = null;
                return false;
            }
            @event = this.EventInstance;
            return true;
        }

        public bool AsOperation(out Operation operation)
        {
            if (this.OperationInstance is null)
            {
                operation = null;
                return false;
            }
            operation = this.OperationInstance;
            return true;
        }

        public override bool Equals(object obj) =>
            obj is Stage stage && this.Id == stage.Id;

        public override int GetHashCode() =>
            this.Id.GetHashCode();
    }

    public enum StageState
    {
        Open,
        InProgress,
        Done,
        Closed
    }

    public class Event : NamedEntity
    {
        public virtual bool      Occured { get; set; }
        public virtual EventType Type    { get; set; } = EventType.OperationResult;

        public void ApplyOriginPolicy()
        {
            this.Occured = this.Type switch
            {
                EventType.OperationResult => false,
                EventType.FromEnvironment => this.Occured,
                _                         => throw new ArgumentOutOfRangeException()
            };
        }
    }

    public enum EventType
    {
        OperationResult,
        FromEnvironment
    }

    public class Operation : NamedEntity
    {
        public virtual ICollection<Artifact> Arguments { get; set; } = new List<Artifact>();
        public virtual ICollection<Artifact> Results   { get; set; } = new List<Artifact>();
    }

    public static class StageBuildExtensions
    {
        public static Gate With(this Stage left, Stage right)
        {
            var gate = new Gate(GateConnectionType.Simple, GateLogicalType.And, left, right);
            left.Gate = gate;
            return gate;
        }

        public static Gate With(this Stage[] left, GateLogicalType type, Stage right)
        {
            var gate = new Gate(GateConnectionType.ManyInputs, type, left.Append(right).ToArray());
            foreach (var item in left)
            {
                item.Gate = gate;
            }
            return gate;
        }

        public static Gate With(this Stage left, GateLogicalType type, params Stage[] right)
        {
            var gate = new Gate(GateConnectionType.ManyOutputs, type, right.Prepend(left).ToArray());
            left.Gate = gate;
            return gate;
        }
    }
}