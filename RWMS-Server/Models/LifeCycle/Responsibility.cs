﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mephi.Cybernetics.RWMS.Server.Models.LifeCycle
{
	public class Responsibility : BaseEntity
	{
		public virtual Person Assignee { get; set; }
		public virtual Operation Operation { get; set; }
	}
}
