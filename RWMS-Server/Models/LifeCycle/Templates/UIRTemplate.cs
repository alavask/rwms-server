﻿using System;
using System.Collections.Generic;
using System.Text;
using Mephi.Cybernetics.RWMS.Server.Models.LifeCycle;

namespace Mephi.Cybernetics.RWMS.Server.Models.LifeCycle.Templates
{
    public static class UIRTemplate
    {
        public static Project Create(
            string   studentName,
            string   teacherName,
            string   consultantName,
            string   studentGroup,
            string   teacherGroup,
            string   consultantGroup,
            DateTime startDate,
            DateTime taskDate,
            DateTime rspzDate,
            DateTime pzDate,
            string   theme
        )
        {
            using var db = new ApplicationContext();
            var student = new Person
            {
                Name = studentName,
                Role = "Студент",
                Group = studentGroup
            };
            var teacher = new Person
            {
                Name = teacherName,
                Role = "Научный руководитель",
                Group = teacherGroup
            };
            var consultant = new Person
            {
                Name = consultantName,
                Role = "Научный консультант",
                Group = consultantGroup
            };
            var taskArtifact = new Artifact
            {
                Name = "Задание на учебно-исследовательскую работу"
            };
            var rspzArtifact = new Artifact
            {
                Name = "Расширенное содержание пояснительной записки"
            };
            var rspzValueArtifact = new Artifact
            {
                Name = "Оценка за расширенное содержание пояснительной записки"
            };
            var pzArtifact = new Artifact
            {
                Name = "Пояснительная записка"
            };
            var pzValueArtifact = new Artifact
            {
                Name = "Оценка за пояснительную записку"
            };
            db.AddRange(
                student,
                teacher,
                consultant,
                rspzArtifact,
                rspzValueArtifact,
                pzArtifact,
                pzValueArtifact
            );
            db.SaveChanges();

            var startEvent = new Stage
            {
                Name = "Начальное событие",
                Description = "Служебное событие, определяющее точку входа в процесс.",
                DueDate = startDate,
                EventInstance = new Event()
            };
            var taskOperation = new Stage
            {
                Name = "Создание задания на УИР",
                Description = "Этап создания задания на УИР.",
                DueDate = taskDate,
                OperationInstance = new Operation
                {
                    Results = new List<Artifact>
                    {
                        taskArtifact
                    }
                }
            };
            var rspzOperation = new Stage
            {
                Name = "Создание РСПЗ",
                Description = "",
                DueDate = rspzDate,
                OperationInstance = new Operation
                {
                    Arguments = new List<Artifact>
                    {
                        taskArtifact
                    },
                    Results = new List<Artifact>
                    {
                        rspzArtifact
                    }
                }
            };
            var rspzValidateOperation = new Stage
            {
                Name = "Проверка РСПЗ",
                Description = "",
                DueDate = rspzDate,
                OperationInstance = new Operation
                {
                    Arguments = new List<Artifact>
                    {
                        rspzArtifact
                    },
                    Results = new List<Artifact>
                    {
                        rspzValueArtifact
                    }
                }
            };
            var rspzErrorEvent = new Stage
            {
                Name = "РСПЗ содержит ошибки",
                Description = "",
                DueDate = rspzDate,
                EventInstance = new Event()
            };
            var rspzDoneEvent = new Stage
            {
                Name = "РСПЗ утверждено",
                Description = "",
                DueDate = rspzDate,
                EventInstance = new Event()
            };
            var pzOperation = new Stage
            {
                Name = "Создание ПЗ",
                Description = "",
                DueDate = pzDate,
                OperationInstance = new Operation
                {
                    Arguments = new List<Artifact>
                    {
                        rspzArtifact
                    },
                    Results = new List<Artifact>
                    {
                        pzArtifact
                    }
                }
            };
            var pzValidateOperation = new Stage
            {
                Name = "Проверка ПЗ",
                Description = "",
                DueDate = pzDate,
                OperationInstance = new Operation
                {
                    Arguments = new List<Artifact>
                    {
                        pzArtifact
                    },
                    Results = new List<Artifact>
                    {
                        pzValueArtifact
                    }
                }
            };
            var pzErrorEvent = new Stage
            {
                Name = "ПЗ содержит ошибки",
                Description = "",
                DueDate = pzDate,
                EventInstance = new Event()
            };
            var pzDoneEvent = new Stage
            {
                Name = "ПЗ утверждено",
                Description = "",
                DueDate = pzDate,
                EventInstance = new Event()
            };
            db.AddRange(
                startEvent,
                taskOperation,
                rspzOperation,
                rspzValidateOperation,
                rspzErrorEvent,
                rspzDoneEvent,
                pzOperation,
                pzValidateOperation,
                pzErrorEvent,
                pzDoneEvent
            );
            db.SaveChanges();

            startEvent.With(taskOperation);
            new[] {taskOperation, rspzErrorEvent}.With(GateLogicalType.Xor, rspzOperation);
            rspzOperation.With(rspzValidateOperation);
            rspzValidateOperation.With(GateLogicalType.Xor, rspzErrorEvent, rspzDoneEvent);
            new[] {rspzDoneEvent, pzErrorEvent}.With(GateLogicalType.Xor, pzOperation);
            pzOperation.With(pzValidateOperation);
            pzValidateOperation.With(GateLogicalType.Xor, pzErrorEvent, pzDoneEvent);
            db.SaveChanges();

            var uir = new Process
            {
                Name = "Учебно-исследовательская работа",
                Description = "",
                Responsibilities = new List<Responsibility>
                {
                    new Responsibility
                    {
                        Assignee = student,
                        Operation = taskOperation.OperationInstance
                    },
                    new Responsibility
                    {
                        Assignee = student,
                        Operation = rspzOperation.OperationInstance
                    },
                    new Responsibility
                    {
                        Assignee = student,
                        Operation = pzOperation.OperationInstance
                    },
                    new Responsibility
                    {
                        Assignee = teacher,
                        Operation = taskOperation.OperationInstance
                    },
                    new Responsibility
                    {
                        Assignee = teacher,
                        Operation = rspzValidateOperation.OperationInstance
                    },
                    new Responsibility
                    {
                        Assignee = teacher,
                        Operation = pzValidateOperation.OperationInstance
                    },
                    new Responsibility
                    {
                        Assignee = consultant,
                        Operation = taskOperation.OperationInstance
                    },
                    new Responsibility
                    {
                        Assignee = consultant,
                        Operation = rspzValidateOperation.OperationInstance
                    },
                    new Responsibility
                    {
                        Assignee = consultant,
                        Operation = pzValidateOperation.OperationInstance
                    }
                },
                FirstStage = startEvent,
                Stages = new List<Stage>()
                {
                    startEvent,
                    taskOperation,
                    rspzOperation,
                    rspzValidateOperation,
                    rspzErrorEvent,
                    rspzDoneEvent,
                    pzOperation,
                    pzValidateOperation,
                    pzErrorEvent,
                    pzDoneEvent
                }
            };

            var project = new Project
            {
                Theme = theme,
                Process = uir
            };
            db.Add(project);
            db.SaveChanges();
            return project;
        }
    }
}