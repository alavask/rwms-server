﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Mephi.Cybernetics.RWMS.Server.Models.LifeCycle
{
    public class Project : BaseEntity
    {
        public virtual string Theme { get; set; }
        public virtual Project Parent { get; set; }
        public virtual Process Process { get; set; }
        public virtual ProjectType Type { get; set; }
    }

    public enum ProjectType
    {
        EducationalResearchWork,
        SummerPractice
    }
}