﻿using System.Collections.Generic;

namespace Mephi.Cybernetics.RWMS.Server.Models.LifeCycle
{
    public class Process : NamedEntity
    {
        public virtual ProcessStatus Status { get; set; } = ProcessStatus.NotStarted;
        public virtual Stage FirstStage { get; set; }
        public virtual ICollection<Stage> Stages { get; set; }
        public virtual ICollection<Responsibility> Responsibilities { get; set; }

        public bool Start()
        {
            if (FirstStage.State != StageState.Open) return false;
            FirstStage.State = StageState.InProgress;
            Status = ProcessStatus.Started;
            Propagate();
            return true;
        }

        public void Propagate()
        {
            while (true)
            {
                var changed = false;
                foreach (var stage in Stages)
                {
                    if (stage is Event && stage.State == StageState.InProgress)
                    {
                        stage.State = StageState.Done;
                    }
                    if (stage.Gate == null)
                    {
                        if (stage.State != StageState.Done) continue;
                        stage.State = StageState.Closed;
                        Status = ProcessStatus.Completed;
                    }
                    else if (stage.Gate.Propagate())
                    {
                        changed = true;
                    }
                }
                if (changed)
                {
                    continue;
                }
                break;
            }
        }
    }

    public enum ProcessStatus
    {
        NotStarted,
        Started,
        Completed
    }
}