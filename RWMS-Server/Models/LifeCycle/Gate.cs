﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Mephi.Cybernetics.RWMS.Server.Models.LifeCycle
{
    public class Gate : BaseEntity
    {
        public virtual List<Stage>        Previous       { get; set; }
        public virtual List<Stage>        Next           { get; set; }
        public virtual GateLogicalType    LogicalType    { get; set; }
        public virtual GateConnectionType ConnectionType { get; set; }

        public Gate(GateConnectionType connectionType, GateLogicalType logicalType, params Stage[] stages)
        {
            this.ConnectionType = connectionType;
            this.LogicalType = logicalType;
            switch (connectionType)
            {
                case GateConnectionType.Simple:
                    this.Previous = new[] {stages[0]}.ToList();
                    this.Next = new[] {stages[1]}.ToList();
                    break;
                case GateConnectionType.ManyInputs:
                    this.Previous = stages[..^1].ToList();
                    this.Next = new[] {stages[^1]}.ToList();
                    break;
                case GateConnectionType.ManyOutputs:
                    this.Previous = new[] {stages[0]}.ToList();
                    this.Next = stages[1..].ToList();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(connectionType), connectionType, null);
            }
        }

        public bool Propagate()
        {
            return this.ConnectionType switch
            {
                GateConnectionType.Simple => SimplePropagate(),
                GateConnectionType.ManyInputs => (
                    this.LogicalType switch
                    {
                        GateLogicalType.And => ManyInputsAndPropagate(),
                        GateLogicalType.Xor => ManyInputsXorPropagate(),
                        GateLogicalType.Or  => ManyInputsOrPropagate(),
                        _                   => throw new ArgumentOutOfRangeException()
                    }),
                GateConnectionType.ManyOutputs => (
                    this.LogicalType switch
                    {
                        GateLogicalType.And => ManyOutputsAndPropagate(),
                        GateLogicalType.Xor => ManyOutputsXorPropagate(),
                        GateLogicalType.Or  => ManyOutputsOrPropagate(),
                        _                   => throw new ArgumentOutOfRangeException()
                    }),
                _ => throw new ArgumentOutOfRangeException()
            };

            bool SimplePropagate()
            {
                if (this.Previous[0].State != StageState.Done) return false;
                this.Previous[0].State = StageState.Closed;
                this.Next[0].State = StageState.InProgress;
                return true;
            }

            bool ManyInputsAndPropagate()
            {
                if (this.Previous.Any(x => x.State != StageState.Done)) return false;
                foreach (var stage in this.Previous)
                {
                    stage.State = StageState.Closed;
                }
                this.Next[0].State = StageState.InProgress;
                return true;
            }

            bool ManyInputsXorPropagate()
            {
                var previous = this.Previous.FirstOrDefault(x => x.State == StageState.Done);
                if (previous == null)
                {
                    return false;
                }
                previous.State = StageState.Closed;
                this.Next[0].State = StageState.InProgress;
                return true;
            }

            bool ManyInputsOrPropagate()
            {
                var doneStages = this.Previous.Where(x => x.State == StageState.Done).ToArray();
                if (!doneStages.Any()) return false;
                foreach (var stage in doneStages)
                {
                    stage.State = StageState.Closed;
                }
                this.Next[0].State = StageState.InProgress;
                return true;
            }

            bool ManyOutputsAndPropagate()
            {
                if (this.Previous[0].State != StageState.Done)
                {
                    return false;
                }
                if (this.Next.All(x => x.EventInstance.Occured))
                {
                    foreach (var stage in this.Next)
                    {
                        stage.State = StageState.Done;
                    }
                }
                this.Previous[0].State = StageState.Closed;
                return true;
            }

            bool ManyOutputsXorPropagate()
            {
                if (this.Previous[0].State != StageState.Done)
                {
                    return false;
                }
                var occuredEvents = this.Next.Where(x => x.EventInstance.Occured).ToArray();
                if (occuredEvents.Length == 0) return false;
                var one = occuredEvents[0];
                one.EventInstance.ApplyOriginPolicy();
                one.State = StageState.Done;
                this.Previous[0].State = StageState.Closed;
                return true;
            }

            bool ManyOutputsOrPropagate() { throw new NotImplementedException(); }
        }
    }

    public enum GateLogicalType
    {
        And,
        Xor,
        Or
    }

    public enum GateConnectionType
    {
        Simple,
        ManyInputs,
        ManyOutputs
    }
}