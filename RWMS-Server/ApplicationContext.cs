﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mephi.Cybernetics.RWMS.Server.Models.Documents;
using Mephi.Cybernetics.RWMS.Server.Models.LifeCycle;
using Microsoft.EntityFrameworkCore;
using Event = Mephi.Cybernetics.RWMS.Server.Models.LifeCycle.Event;
using Operation = Mephi.Cybernetics.RWMS.Server.Models.LifeCycle.Operation;
using Task = Mephi.Cybernetics.RWMS.Server.Models.Tasks.Task;

namespace Mephi.Cybernetics.RWMS.Server
{
	public class ApplicationContext : DbContext
	{
		public DbSet<Artifact> Artifacts { get; set; }
		public DbSet<Person> Persons { get; set; }
		public DbSet<Event> Events { get; set; }
		public DbSet<Operation> Operations { get; set; }
		public DbSet<Responsibility> Responsibilities { get; set; }
		public DbSet<Project> Projects { get; set; }
		public DbSet<RSPZ> RSPZs { get; set; }
		public DbSet<PZ> PZs { get; set; }
		public DbSet<Task> Tasks { get; set; }

		private static bool wasCreated = false;

		public ApplicationContext()
		{
			if (wasCreated) return;
			_ = this.Database.EnsureDeleted();
			wasCreated = true;
			_ = this.Database.EnsureCreated();
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			/*(localdb)\\mssqllocaldb*/
			_ = optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=RWMS;Trusted_Connection=True;");
		}
	}
}
