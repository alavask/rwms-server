using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mephi.Cybernetics.RWMS.Server.Models.Documents.Templates;
using Mephi.Cybernetics.RWMS.Server.Models.LifeCycle.Templates;
using Mephi.Cybernetics.RWMS.Server.Models.Tasks.Templates;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Mephi.Cybernetics.RWMS.Server
{
	public class Program
	{
		public static void Main(string[] args)
		{
			_ = UIRTemplate.Create(
				"Маштак И.А.",
				"Рословцев В.В.",
				"",
				"Б16-504",
				"Кафедра 22",
				"",
				new DateTime(2019, 9, 1),
				new DateTime(2019, 9, 14),
				new DateTime(2019, 10, 11),
				new DateTime(2019, 12, 22),
				"Проектирование и разработка подсистемы управления жизненным циклом проектов для системы управления НИР"
				);
			_ = UIRTemplate.Create(
				"Маштак И.А.",
				"Короткова М.А.",
				"",
				"Б16-504",
				"Кафедра 22",
				"",
				new DateTime(2019, 2, 1),
				new DateTime(2019, 2, 14),
				new DateTime(2019, 3, 11),
				new DateTime(2019, 5, 22),
				"Применение алгоритма поиска скрытой периодичности в символьных " +
					"последовательностях к анализу числовых рядов сигналов электроэнцефалограмм"
				);
            _ = RSPZTemplate.Create();
			_ = UIRTaskTemplate.CreateDefault();
			CreateHostBuilder(args).Build().Run();
		}

		public static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args)
				.ConfigureWebHostDefaults(webBuilder =>
				{
					webBuilder.UseStartup<Startup>();
				});
	}
}
