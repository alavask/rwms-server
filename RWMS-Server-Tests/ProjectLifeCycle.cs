using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Mephi.Cybernetics.RWMS.Server.Controllers.Projects;
using Mephi.Cybernetics.RWMS.Server.Models.LifeCycle;
using Microsoft.EntityFrameworkCore;

namespace Mephi.Cybernetics.RWMS.Server.Tests
{
    public class ProjectLifeCycle
    {
        [Fact]
        public void SimpleLife()
        {
            var projectController = new ProjectController();
            var projectId = projectController.CreateUIR(new UIRCreationDto(
                "Маштак И.А.",
                "Б16-504",
                "Рословцев В.В.",
                "Кафедра 22",
                "",
                "",
                "Очень длинная тема УИРа",
                ProjectType.EducationalResearchWork,
                new DateTime(2019, 9, 1),
                new DateTime(2019, 9, 14),
                new DateTime(2019, 10, 20),
                new DateTime(2019, 12, 23)
            ));
            var started = projectController.Start(projectId);
            Assert.True(started);

            List<Stage> stages;
            using (var ctx = new ApplicationContext())
            {
                stages = ctx.Projects.AsNoTracking()
                    .Include(p => p.Process)
                    .ThenInclude(p => p.Stages)
                    .ThenInclude(s => s.Gate)
                    .Where(p => p.Id == projectId)
                    .SelectMany(p => p.Process.Stages)
                    .ToList();
            }

            var startEvent = stages.FirstOrDefault(s => s.Name == "Начальное событие");
            Assert.NotNull(startEvent);
            var taskOperation = stages.FirstOrDefault(s => s.Name == "Создание задания на УИР");
            Assert.NotNull(taskOperation);
            var rspzOperation = stages.FirstOrDefault(s => s.Name == "Создание РСПЗ");
            Assert.NotNull(rspzOperation);
            var rspzValidateOperation = stages.FirstOrDefault(s => s.Name == "Проверка РСПЗ");
            Assert.NotNull(rspzValidateOperation);
            var rspzErrorEvent = stages.FirstOrDefault(s => s.Name == "РСПЗ содержит ошибки");
            Assert.NotNull(rspzErrorEvent);
            var rspzDoneEvent = stages.FirstOrDefault(s => s.Name == "РСПЗ утверждено");
            Assert.NotNull(rspzDoneEvent);
            var pzOperation = stages.FirstOrDefault(s => s.Name == "Создание ПЗ");
            Assert.NotNull(pzOperation);
            var pzValidateOperation = stages.FirstOrDefault(s => s.Name == "Проверка ПЗ");
            Assert.NotNull(pzValidateOperation);
            var pzErrorEvent = stages.FirstOrDefault(s => s.Name == "ПЗ содержит ошибки");
            Assert.NotNull(pzErrorEvent);
            var pzDoneEvent = stages.FirstOrDefault(s => s.Name == "ПЗ утверждено");
            Assert.NotNull(pzDoneEvent);

            void SendStage(int sendOperationId, int achieveOperationId, int raiseEventId)
            {
                projectController.CompleteOperationWithEvent(projectId, sendOperationId, raiseEventId);
                var activeStages = projectController.GetActiveStages(projectId);
                Assert.Single(activeStages);
                var activeStage = activeStages.Single();
                Assert.Equal(achieveOperationId, activeStage.Id);
            }
            
            SendStage(taskOperation.Id, rspzOperation.Id, -1);
            SendStage(rspzOperation.Id, rspzValidateOperation.Id, -1);
            SendStage(rspzValidateOperation.Id, pzOperation.Id, rspzDoneEvent.Id);
            SendStage(pzOperation.Id, pzValidateOperation.Id, -1);
            SendStage(pzValidateOperation.Id, pzOperation.Id, pzErrorEvent.Id);
            SendStage(pzOperation.Id, pzValidateOperation.Id, -1);
            
            projectController.CompleteOperationWithEvent(projectId, pzValidateOperation.Id, pzDoneEvent.Id);
            var empty = projectController.GetActiveStages(projectId);
            Assert.Empty(empty);
            using (var ctx = new ApplicationContext())
            {
                var project = ctx.Projects.AsNoTracking()
                    .Include(p => p.Process)
                    .ThenInclude(p => p.Stages)
                    .FirstOrDefault(p => p.Id == projectId);
                Assert.NotNull(project);
                Assert.Equal(ProcessStatus.Completed, project.Process.Status);
            }
        }
    }
}